# README #

This repository contains code examples for my book [XML processing and website scraping in Java](https://leanpub.com/javaxml).

## Requirements

This project requires **Java 8** at least.