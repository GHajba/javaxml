package hu.japy.dev.xml_parser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * This example class shows how to parse XML files with JAX-B
 *
 * @author GHajba
 */
public class JAXB {
    public static <T> T unmarshal(final String fileName, final Class<T> targetType) throws JAXBException,
            XMLStreamException, FactoryConfigurationError, FileNotFoundException, IOException {

        final JAXBContext jc = JAXBContext.newInstance(targetType);
        final Unmarshaller unmarshaller = jc.createUnmarshaller();
        // final ;
        try (FileReader fileReader = new FileReader(fileName)) {
            final XMLStreamReader xmlReader = XMLInputFactory.newInstance().createXMLStreamReader(fileReader);
            try {
                return unmarshaller.unmarshal(xmlReader, targetType).getValue();
            } finally {
                xmlReader.close();
            }
        }
    }

    public static void main(String... args) {
        try {
            final Publications publications = unmarshal("books.xml", Publications.class);
            for (final Book b : publications.books) {
                System.out.println(b);
            }

        } catch (JAXBException | XMLStreamException | FactoryConfigurationError | IOException e) {
            e.printStackTrace();
        }
    }
}
