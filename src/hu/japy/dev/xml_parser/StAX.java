package hu.japy.dev.xml_parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

/**
 * This example class shows how to parse XML files with StAX.
 *
 * @author GHajba
 */
public class StAX {

    public static void main(String[] args) throws XMLStreamException, FileNotFoundException {

        final XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        final InputStream in = new FileInputStream("books.xml");
        final XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            if (event.isEndElement()) {
                if (event.asEndElement().getName().getLocalPart().equals("book")) {
                    event = eventReader.nextEvent();
                    System.out.println("=================================");
                    continue;
                }
            }
            if (event.isStartElement()) {
                if (event.asStartElement().getName().getLocalPart().equals("title")) {
                    event = eventReader.nextEvent();
                    System.out.println("title: " + event.asCharacters().getData());
                    continue;
                }
                if (event.asStartElement().getName().getLocalPart()
                        .equals("author")) {
                    event = eventReader.nextEvent();
                    System.out.println("author: " + event.asCharacters().getData());
                    continue;
                }
                if (event.asStartElement().getName().getLocalPart().equals("copyright")) {
                    event = eventReader.nextEvent();
                    System.out.println("copyright: " + event.asCharacters().getData());
                    continue;
                }
                if (event.asStartElement().getName().getLocalPart().equals("publisher")) {
                    event = eventReader.nextEvent();
                    System.out.println("publisher: " + event.asCharacters().getData());
                    continue;
                }
                if (event.asStartElement().getName().getLocalPart().equals("isbn")) {
                    event = eventReader.nextEvent();
                    System.out.println("isbn: " + event.asCharacters().getData());
                    continue;
                }
            }
        }
    }

}
