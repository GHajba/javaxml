package hu.japy.dev.xml_parser;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This example class shows how to parse XML files with SAX.
 *
 * @author GHajba
 */
public class SAX {

    public static void main(String... args) {
        try {
            final File inputFile = new File("books.xml");
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            final SAXParser saxParser = factory.newSAXParser();
            saxParser.setProperty(
                    "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                    "http://www.w3.org/2001/XMLSchema");
            saxParser.parse(inputFile, new BookHandler());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}

class BookHandler extends DefaultHandler {

    @Override
    public void startElement(String namespaceURI, String localName,
            String qName, Attributes atts)
            throws SAXException {
        if ("book".equals(qName)) {
            System.out.print("Book details: Book ID: " + atts.getValue("id"));
        }
        else {
            System.out.print(qName + ": ");
        }
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        System.out.print(new String(ch, start, length));
    }

    @Override
    public void endElement(String namespaceURI, String localName,
            String qName)
            throws SAXException {
        if ("book".equals(qName)) {
            System.out.println("=================================");
        }
    }

}
