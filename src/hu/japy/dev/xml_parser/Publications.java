package hu.japy.dev.xml_parser;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * Sample parent class to extract Book objects with JAXB.
 *
 * @author GHajba
 *
 */
public class Publications {

    @XmlElement(name = "book")
    List<Book> books;

    public List<Book> getBooks() {
        return this.books;
    }
}
