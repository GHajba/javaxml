package hu.japy.dev.xml_parser;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * This is the sample class we will fill with data from the books.xml file for the examples.
 *
 * @author GHajba
 *
 */
public class Book {

    @XmlAttribute
    private String id;
    private String title;
    private String author;
    private int copyright;
    private String publisher;
    private long isbn;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCopyright() {
        return this.copyright;
    }

    public void setCopyright(int copyright) {
        this.copyright = copyright;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public long getIsbn() {
        return this.isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        final StringBuilder stringRepresentation = new StringBuilder();
        final String newLine = System.getProperty("line.separator");
        stringRepresentation.append(this.getClass().getName() + " {" + newLine);
        stringRepresentation.append(" ID: " + this.id + newLine);
        stringRepresentation.append(" Title: " + this.title + newLine);
        stringRepresentation.append(" Copyright: " + this.copyright + newLine);
        stringRepresentation.append(" Publisher: " + this.publisher + newLine);
        stringRepresentation.append(" ISBN: " + this.isbn + newLine);
        stringRepresentation.append("}");
        return stringRepresentation.toString();
    }
}
